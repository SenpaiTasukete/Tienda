package org.neoforex.tienda.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "email", nullable = false)
    @Email(message = "*Introdueixi un email correcta.")
    @NotEmpty(message = "*Siusplau introdueixi un email.")
    private String email;

    @Column(name = "password", nullable = false)
    @Length(min = 5, message = "*La contrasenya te de tindre minim 5 caracters.")
    @NotEmpty(message = "*Siusplau introdueixi una contrasenya.")
    private String password;

    @Column(name = "name")
    @NotEmpty(message = "*Siusplau introdueixi el seu noum.")
    private String name;

    @Column(name = "lastname")
    @NotEmpty(message = "*Siusplau introdueixi el seu cognom.")
    private String lastname;

    @Column(name="account_active")
    private int active;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Role role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
