package org.neoforex.tienda.repository;

import org.neoforex.tienda.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findById(int id);
    User findByEmail(String email);
}
