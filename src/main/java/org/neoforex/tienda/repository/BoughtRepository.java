package org.neoforex.tienda.repository;

import org.neoforex.tienda.entity.Bought;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface BoughtRepository extends JpaRepository<Bought, Long> {

    ArrayList<Bought> findAll();
    ArrayList<Bought> findAllByUser(Long id);
}
