package org.neoforex.tienda.repository;

import org.neoforex.tienda.entity.Product;
import org.neoforex.tienda.entity.Shopping;
import org.neoforex.tienda.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface ShoppingRepository extends JpaRepository<Shopping, Long> {

    ArrayList<Shopping> findAll();
    ArrayList<Shopping> findAllByUser(User id);
    Shopping findByUserAndProduct(User user, Product product);
}
