package org.neoforex.tienda.service;

import org.neoforex.tienda.entity.Product;
import org.neoforex.tienda.entity.Shopping;
import org.neoforex.tienda.entity.User;
import org.neoforex.tienda.repository.ShoppingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ShoppingService {

    @Autowired
    private ShoppingRepository shoppingRepository;

    public ArrayList<Shopping> findByUser(User id) {
        return this.shoppingRepository.findAllByUser(id);
    }

    public Shopping findByUserAndProduct(User user, Product product) {
        return this.shoppingRepository.findByUserAndProduct(user, product);
    }

    public void save(Shopping shopping) {
        this.shoppingRepository.save(shopping);
    }
}
