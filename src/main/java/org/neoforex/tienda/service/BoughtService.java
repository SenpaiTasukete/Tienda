package org.neoforex.tienda.service;

import org.neoforex.tienda.entity.Bought;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class BoughtService {

    @Autowired
    private BoughtService boughtService;

    public ArrayList<Bought> findByUser(long id) {
        return this.boughtService.findByUser(id);
    }

    public void save(Bought bought) {
        this.boughtService.save(bought);
    }
}
