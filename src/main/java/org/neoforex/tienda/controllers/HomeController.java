package org.neoforex.tienda.controllers;

import org.neoforex.tienda.entity.User;
import org.neoforex.tienda.objects.Message;
import org.neoforex.tienda.service.ProductService;
import org.neoforex.tienda.service.ShoppingService;
import org.neoforex.tienda.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class HomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ShoppingService shoppingService;

    @RequestMapping("/")
    public String home(Model model) {
        model.addAttribute("products", productService.findAll());
        return "index";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model) {
        model.addAttribute("user", new User());
        return "auth/register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerValidator(@Valid User user, BindingResult bindingResult, Model model,
                                    RedirectAttributes redirectAttributes) {
        User user1 = userService.findByEmail(user.getEmail());
        if(user1 != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided");
        }

        if(bindingResult.hasErrors()) {
            model.addAttribute("user", user);
            model.addAttribute("message", new Message("ui negative message",
                    "Error",
                    "Comproba el correu i les dades introduides."));
            return "auth/register";
        } else {
            userService.save(user);
            redirectAttributes.addFlashAttribute("message",
                    new Message("ui positive message",
                            "Tot correcta!",
                            "El teu usuari ha sigut creat!"));
            return "redirect:/login";
        }
    }
}
