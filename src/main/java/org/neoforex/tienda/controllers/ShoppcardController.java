package org.neoforex.tienda.controllers;

import org.neoforex.tienda.entity.Product;
import org.neoforex.tienda.entity.Shopping;
import org.neoforex.tienda.entity.User;
import org.neoforex.tienda.service.ProductService;
import org.neoforex.tienda.service.ShoppingService;
import org.neoforex.tienda.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;

@Controller
@RequestMapping("/shopping")
public class ShoppcardController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ShoppingService shoppingService;

    @Autowired
    private UserService userService;

    @RequestMapping("/cart")
    public String cart(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();

        User user = userService.findByEmail(name);
        ArrayList<Shopping> shopping = shoppingService.findByUser(user);
        int total = shopping.stream().mapToInt(e -> (int) e.getProduct().getPrice()*e.getCuantity()).sum();

        model.addAttribute("shopping", shopping);
        model.addAttribute("total", total);

       return "shopping/cart";
    }

    @RequestMapping("/add/{id}")
    public String add(@PathVariable int id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        User user = userService.findByEmail(name);
        Product product = productService.findById(id);

        Shopping exist = shoppingService.findByUserAndProduct(user, product);


        System.out.println("LEL!");
        if(exist != null) {
            exist.setCuantity(exist.getCuantity()+1);
            System.out.println("Lol!");
            shoppingService.save(exist);
        } else {
            Shopping shopping = new Shopping();
            shopping.setProduct(product);
            shopping.setUser(user);
            shopping.setCuantity(1);
            shoppingService.save(shopping);
        }

        return "redirect:/shopping/cart";
    }
}
